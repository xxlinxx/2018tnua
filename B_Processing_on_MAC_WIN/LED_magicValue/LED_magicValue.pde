//assume we have 12 led on the circuit board,
//simulate it on software. YSLin
int LED_NUMBER=25;
byte leds[]=new byte[LED_NUMBER];
int ellipse_r=33;
float deltaT=TWO_PI/1000;
float thetaAngle=0;
float thetaAngleSpeed=0.01;
void setup(){
size(1024,100);
}
void draw(){
  settingValue_Method();
  showValues();
  drawLED();
  thetaAngle+=thetaAngleSpeed;
}
void showValues(){
  print(printBB("LEDs=",leds));
}

void settingValue_Method(){
  for(int i=0;i<leds.length;i++){
    //leds[i]++;
    //leds[i]=(byte)(127*(1+sin(deltaT*millis() +i+(TWO_PI/LED_NUMBER) )));
    leds[i]=(byte)(127*(1+sin(thetaAngle+millis()*deltaT)));
    //leds[i]=(byte)pow(leds[i],0.1);
    //leds[i]=(byte)(127*(1+sin(0.1*i+deltaT*millis())*0.5 ));
  }
}

void drawLED(){
  for(int i=0;i<leds.length;i++){
    //int a=0*leds[i]+128<<24;
    //byte r=(byte)(0xFF&(leds[i])<<16);
    //byte g=(byte)(0xFF&(leds[i])<<8);
    //byte b=(byte)(0xFF&(leds[i]));
    //fill(128+(r|g|b));
    fill(int(leds[i]));
    ellipse(ellipse_r+(i*ellipse_r), ellipse_r, ellipse_r,ellipse_r);  
  }
}

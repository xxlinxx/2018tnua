void setup() {
  Multithread mt = new Multithread(); 
  for(int fID=0;fID<=100;fID++)
    new Thread(mt, "fans_"+fID).start(); 
}
void draw() {
  background(127*(1+sin(TWO_PI/1000*millis())));
}


class Multithread implements Runnable { 
  private int ticket = 2000;
  private int totalSell=0;
  Object lock = new Object(); 
  @Override 
    public void run() 
  { 
    //synchronized(lock){
    saleTicket();
    //}
  } 
  public void saleTicket() { 
    while (true) { 
      
      //synchronized(lock) {
        if (ticket > 0) { 
          System.out.println(Thread.currentThread().getName() + " 正在賣第" + ticket + "張票");
          ticket--;
          totalSell++;
        } else { 
          System.out.println("no tickets! @"+Thread.currentThread().getName() +" totalSell="+totalSell); 
          break;
        }
        sleepsleep(1);
      }
    //}
  }
  void sleepsleep(int s) {
    try { 
      Thread.sleep(s);
    } 
    catch (InterruptedException e) { 
      e.printStackTrace();
    }
  }
}

RTIME=3
LESSTIME=$RTIME
while [ true ]; do
 if [ "$LESSTIME" = "0" ]; then
 VAR="`date '+%Y-%m-%d-%H:%M:%S'`"
 git add .
 git commit -m "${VAR} - commit auto"
 LESSTIME=$RTIME
 fi
 sleep 1
 echo $LESSTIME
 LESSTIME=$(($LESSTIME-1))
done
